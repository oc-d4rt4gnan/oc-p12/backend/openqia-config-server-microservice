## Table of Contents
1. [General Info](#general-info)
2. [Technologies](#technologies)
3. [Prerequisites](#prerequisites)
4. [Installation](#installation)
5. [Configuration](#configuration)
6. [Usage](#usage)
### General Info
***
Microservice for managing articles.

It allows you to add, select and update items that will be found on documents (quotes and invoices).

It is part of a set of Microservices forming the OpenQIA project.

The code is evolving, it is currently in its first version.
## Technologies
***
A list of technologies used within the project:
* [Spring Framework](https://spring.io/projects/spring-framework): Version 2.6.1 
* [Maven](https://maven.apache.org): Version 3.6.0
* [Git](https://git-scm.com): Version 2.7.4
* [Java](https://www.java.com): Version 1.8.0_201
## Prerequisites
You need to create a private distant repo with 3 files:
* openqia-company.properties
* openqia-items.properties
* openqia-document.properties
Each one need to contains information like
```
server.port=(set the port you want and different for each microservice)

### Default Connection Pool ###
spring.datasource.hikari.connectionTimeout=20000
spring.datasource.hikari.maximumPoolSize=5

### PostgreSQL ###
spring.datasource.url=(jdbc uri of the database)
spring.jpa.properties.hibernate.default_schema=(pgitems for items microservice, pgdocuments for documents microservice and pgcompany for company microservice)
spring.datasource.username=(owner of the database)
spring.datasource.password=(owner password of the database)
spring.datasource.driver-class-name=org.postgresql.Driver

### SpringBoot Actuator ###
management.endpoints.web.exposure.include=refresh
```
## Installation
***
For the installation, it is enough to clone the project 
```
$ git clone https://gitlab.com/oc-d4rt4gnan/oc-p12/backend/openqia-config-server-microservice.git
```
as well as the other necessary microservices (in particular the **configuration server**):
* [Items Microservice](https://gitlab.com/oc-d4rt4gnan/oc-p12/backend/openqia-items-microservice)
* [Company Microservice](https://gitlab.com/oc-d4rt4gnan/oc-p12/backend/openqia-company-microservice)
* [DocumentMicroservice](https://gitlab.com/oc-d4rt4gnan/oc-p12/backend/openqia-documents-microservice)
Then, either to launch the project directly from an IDE thanks to **spring boot**, or to compile with **Maven**
```
$ cd /path_to_file/openqia.config.server

$ mvn clean install
```
and to put the **jar** created form target folder into **tomcat server**. 
## Configuration
***
Create an application.properties file and fill it with :
```
spring.cloud.compatibility-verifier.enabled=false
spring.application.name=config-server
server.port=9101 (you change it)
spring.cloud.config.server.git.uri=(put your distant repo url)
spring.cloud.config.server.git.username=(the username to access the distant repo)
spring.cloud.config.server.git.password=(the password to access the distant repo)
```
## Usage
***
You can use the [Front end](https://gitlab.com/oc-d4rt4gnan/oc-p12/frontend) associated or create your own front end by using the yaml in **src/main/resources** folder.
